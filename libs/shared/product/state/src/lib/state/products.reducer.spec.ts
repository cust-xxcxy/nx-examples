import { mockProducts } from '@nx-example/shared/product/data/testing';

import { ProductsState } from "./products.reducer";

describe('Products Reducer', () => {
  let productsState: ProductsState;

  beforeEach(() => {
    productsState = {
      products: mockProducts
    };
  });

  describe('unknown action', () => {
    it('should return the initial state', () => {
      expect(productsState).toBe(productsState);
    });
  });
});
